from time import time

import bruteforce
import stackoverflow
import wikipedia


def main():
    # x_list = [100, 75, 15, 495, 995, 995, 995, 995, 510, 110]
    # target = 780
    x_list = [10, 15, 7, 5, 6]
    target = 27
    # x_list = [1150, 495, 995, 995, 995, 995, 100, 750, 3305, 75, 510, 3265, 2145, 1935, 140, 140, 15, 1330, 2800, 1250,
    #           350, 850, 110]
    # target = 8270

    time0 = time()
    bf = bruteforce.bruteforce(x_list, target)
    time1 = time()
    so = stackoverflow.stackoverflow(x_list, target)
    time2 = time()
    wi = wikipedia.approx_with_accounting_and_duplicates(x_list, target)
    time3 = time()

    print('Полный перебор:', bf, time1 - time0)
    print('Аппроксимационный алгоритм:', wi, time3 - time2)
    print('Точный для точного L:', so, time2 - time1)


if __name__ == '__main__':
    main()
